Friends Jam II

Theme:
Observe the Fire from the Opposite Shore

Title:
River Fire.

Concept:
You throw (1) molotov across the river and Observe the Fire from the Opposite Shore.
You can prepare by using tools to break bridge, cut down trees, etc...
Use the mechanics to their full potential to maximize points! (# of things caught on fire)
But don't be too greedy or you might just end up in the fire yourself and that's a GAME OVER!

You would be scored by the number of ground tiles you've burned. (% of island engulfed)
Explosives could give more points, rewarding risky play.
Bonus missions: light all bonfires!

Controls:
W - Move Forward
S - Move Backward
D - Strafe Right
A - Strafe Left

Up Arrow - Look Up
Down Arrow - Look Down
Right Arrow - Look Right
Left Arrow - Look Left

E - Throw Molotov
Enter - Break Nearest Bridge

Menu Specific:
Enter - Confirm
Escape - Cancel
