#include "explosive.h"
#include "jamutils.h"
#include "camera_fx.h"
#include "particles.h"

void system_explosive(hge_entity* entity, hge_vec3* position, explosive_component* explosive) {
  //printf("Here's the bool: %s\n", isPositionOverGround(*position) ? "Yes" : "No");
  if(!isPositionOverGround(*position)) {
    float spread = 10;
    for(int i = 0; i < 25; i++) {
      hge_entity* proj = create_projectile(hgeResourcesQueryMesh("fire proj 1"), *position,
      hgeVec3(RandomFloat(-spread, spread), RandomFloat(5, 15), RandomFloat(-spread, spread)));
      particle_system ps;
    	ps.num_particles = 0;
    	ps.particle_mesh = hgeResourcesQueryMesh("particle");
    	ps.particle_mesh.material.diffuse = hgeResourcesQueryTexture("image");
    	hgeAddComponent(proj, hgeCreateComponent("particle system", &ps, sizeof(ps)));
    }
    shake_camera(5.f);
    hgeDestroyEntity(entity);
  }
}
