#ifndef LEVEL_H
#define LEVEL_H

#define TILE_STATE_INVINCIBLE 0
#define TILE_STATE_GROUND 1
#define TILE_STATE_FIRE 2

// 4
#define TILESIZE 4

#include <HGE/HGE_Core.h>
#include "gamestate.h"
#include "particles.h"
#include "bridge.h"
#include "explosive.h"
#include "level_selector.h"

typedef struct {
	hge_mesh mesh;
} c_mesh;

typedef struct {
	hge_vec3 target_position;
	float speed;
	float anim_timer;
	int anim_state;
	float score;
	float time;
} player_controller;

typedef struct {
  //hge_entity* entities;
	float step_timer;
	float frequency;
	float level_loading_timer;
} level_manager;

void ReloadLevel();
void LoadLevel(const char* level_path);
void UnloadLevel();

char* cur_level();

void add_to_level_entities(hge_entity* entity);

void level_modify_ground_state(int x, int y, int state);

void system_level_manager(hge_entity* entity, level_manager* level);

bool isPositionInLevel(hge_vec3 position);
bool isPositionOverGround(hge_vec3 position);
int tileAtPosition(hge_vec3 position);
float level_precentage();

#endif
