#ifndef BRIDGE_H
#define BRIDGE_H

#include <HGE/HGE_Core.h>
#include <math.h>

typedef struct {

} bridge_component;

void system_bridge(hge_entity* entity, hge_vec3* position, bridge_component* bridge_c);

#endif
