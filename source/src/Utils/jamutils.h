#ifndef JAM_UTILS_H
#define JAM_UTILS_H
#include <stdlib.h>
#include <stdint.h>
#include <HGE/HGE_Core.h>

typedef uint8_t *pointer;

typedef struct {
  int hours, minutes, seconds, miliseconds;
} jam_time;

float distance(hge_vec3 position_a, hge_vec3 position_b);
float RandomFloat(float a, float b);
float easeinout(float a, float b, float t);

void binwrite(const char* file_name, pointer data, size_t size);
void binread(const char* file_name, pointer data, size_t size);

jam_time float_to_int_time(float time);

#endif
