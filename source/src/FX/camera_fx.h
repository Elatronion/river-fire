#ifndef CAMERA_FX_H
#define CAMERA_FX_H
#include <HGE/HGE_Core.h>

typedef struct {
  hge_vec3 desired_position;
  hge_vec3 shake_vector;
  float shake_intensity;
} camera_fx;

void shake_camera(float ammount);

void System_CameraFX(hge_entity* entity, hge_vec3* position, camera_fx* camfx);

#endif
