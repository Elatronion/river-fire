#ifndef LEVEL_SELECTOR_H
#define LEVEL_SELECTOR_H
#include <HGE/HGE_Core.h>

typedef struct {
  int anim_id;
  float anim_timer;
  int selected_level;
  float last_y;
} level_selector;

typedef struct {
  float score;
  float time;
} level_data;

void clean_loaded_level_files();
void load_level_folder();

void system_level_selector(hge_entity* entity, level_selector* selector, hge_vec3* position, orientation_component* orientation);

#endif
