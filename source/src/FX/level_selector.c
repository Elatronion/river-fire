#include "level_selector.h"
#include "level.h"
#include "text.h"
#include "jamutils.h"
#include "dirent.h"

typedef struct level_file {
  char title[255];
  level_data lvl_data;
  struct level_file * next;
} level_file_t;

level_file_t* head = NULL;

void sort_level_list() {
  bool ordered = false;
	level_file_t* cur = head;
  while(!ordered) {
    ordered = true;
    cur = head;
    while(cur) {

      level_file_t* FIRST = cur;
      level_file_t* SECOND = NULL;
      level_file_t* THIRD = NULL;

      if(FIRST)
        SECOND = FIRST->next;
      if(SECOND)
        THIRD = SECOND->next;

      if(SECOND && THIRD)
      if(SECOND->title[0] > THIRD->title[0]) {
        // Swap
        SECOND->next = THIRD->next;
        THIRD->next = SECOND;
        FIRST->next = THIRD;
        ordered = false;
      }

      cur = cur->next;
    }
  }
  printf("DONE!\n");
}

int num_level_files() {
	int count = 0;
	level_file_t* cur = head;
	while(cur->next) {
		cur = cur->next;
		count++;
	}
	return count;
}

char* get_level_title(int index) {
	int count = 0;
	level_file_t* cur = head;
	while(cur->next) {
		cur = cur->next;
		if(count == index) break;
		count++;
	}
	return &cur->title;
}

level_data get_level_data(int index) {
	int count = 0;
	level_file_t* cur = head;
	while(cur->next) {
		cur = cur->next;
		if(count == index) break;
		count++;
	}
	return cur->lvl_data;
}

level_data load_data_file(const char* title) {
  level_data lvl_data = { 0.0f, 0.0f };
  char path[255];
  sprintf(path, "%s%s%s", "res/levels/scores/", title, ".bin");
  binread(path, &lvl_data, sizeof(lvl_data));
  return lvl_data;
}

void add_level_file(const char* title) {
	//title[strlen(title)-3] = "\0";
  if(!strcmp(title, "level selection.tmx")) return;
	printf("Found Level File '%s'\n", title);
	level_file_t* cur = head;
	while(cur->next)
		cur = cur->next;
	cur->next = (level_file_t*)malloc(sizeof(level_file_t));
	cur = cur->next;
	strcpy(cur->title, title);
	cur->title[strlen(cur->title)-4] = '\0';
  cur->lvl_data = load_data_file(cur->title);
	cur->next = NULL;
}

void clean_loaded_level_files() {
	level_file_t* cur = head->next;
	while(cur) {
		level_file_t* tmp = cur;
		cur = cur->next;
		free(tmp);
	}
	free(head);
}

// TODO: Reorder levels alphabetically
void load_level_folder() {
	head = (level_file_t*)malloc(sizeof(level_file_t));
	head->next = NULL;

	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir ("res/levels/")) != NULL) {
		while ((ent = readdir (dir)) != NULL) {
			char *dot = strrchr(ent->d_name, '.');
			if (dot && !strcmp(dot, ".tmx"))
				add_level_file(ent->d_name);
		}
		closedir (dir);
	} else {
		perror ("");
		return EXIT_FAILURE;
	}

  sort_level_list();
}

void system_level_selector(hge_entity* entity, level_selector* selector, hge_vec3* position, orientation_component* orientation) {
	// CAMERA SWEEP
	position->y = 5;
	position->x = cos(hgeMathRadians(hgeRuntime()*10)) * 10 + 8*TILESIZE;
	position->z = sin(hgeMathRadians(hgeRuntime()*10)) * 10 + 8*TILESIZE;
	orientation->yaw = hgeRuntime() * 10 - 180;

	// GUI
  selector->anim_timer += hgeDeltaTime();

  int num_levels = num_level_files();

  char score_time_str[50];

  float ammount;
  switch (selector->anim_id) {
    case 0:
    ammount = easeinout(0, 1, fmin(1.0f, selector->anim_timer/1.5f));
    jamTextRenderSimple("River Fire.", true, hgeVec3(0, 0, 0), 1.0f, hgeVec4(1, 1, 1, ammount));
    if(selector->anim_timer >= 1.75f) {
      selector->anim_id++;
      selector->anim_timer = 0;
    }
    break;
    case 1:
      ammount = easeinout(0, 200, fmin(1.0f, selector->anim_timer/1.f));
      jamTextRenderSimple("River Fire.", true, hgeVec3(0, ammount, 0), 1.0f, hgeVec4(1, 1, 1, 1));
      if(selector->anim_timer >= 1.f) {
        selector->anim_id++;
        selector->anim_timer = 0;
      }
    break;
    case 2:
      ammount = easeinout(0, 1, fmin(1.0f, selector->anim_timer/1.25f));
      jamTextRenderSimple("River Fire.", true, hgeVec3(0, 200, 0), 1.0f, hgeVec4(1, 1, 1, 1));
      for(int i = 0; i < num_levels; i++) {
        float text_y = (selector->selected_level*50) + i*-50;
        jamTextRenderSimple(get_level_title(i), true, hgeVec3(0, text_y, 0), 1.0-fabs(text_y)/350.f, hgeVec4(1, 1, 1, (1.0-fabs(text_y)/150.f)*ammount));
        level_data lvl_data = get_level_data(i);
        jam_time jt = float_to_int_time(lvl_data.time);
        sprintf(score_time_str, "%.2f%s %02d:%02d.%03d", lvl_data.score, "%", jt.minutes, jt.seconds, jt.miliseconds);
        jamTextRenderSimple(score_time_str, false, hgeVec3(25 + jamTextLengthOfText(get_level_title(i), 1.0-fabs(text_y)/350.f)/2, text_y+5, 1), 0.5-fabs(text_y)/350.f, hgeVec4(1, 1, 1, (0.75-fabs(text_y)/150.f)*ammount));
      }
      if(selector->anim_timer >= 1.25f) {
        selector->anim_id++;
        selector->anim_timer = 0;
      }
    break;
    case 3:
      if(selector->anim_timer >= 0.25f)
      if(selector->selected_level > 0 && (hgeInputGetKey(HGE_KEY_W) || hgeInputGetKey(HGE_KEY_UP))) {
        selector->last_y = (selector->selected_level*50);
        selector->selected_level--;
        selector->anim_timer=0;
      } else if(selector->selected_level < num_levels-1 && (hgeInputGetKey(HGE_KEY_S) || hgeInputGetKey(HGE_KEY_DOWN))) {
        selector->last_y = (selector->selected_level*50);
        selector->selected_level++;
        selector->anim_timer=0;
      } else if(hgeInputGetKeyDown(HGE_KEY_ENTER)) {
				char level_to_load[255];
				sprintf(level_to_load, "%s%s%s", "res/levels/", get_level_title(selector->selected_level), ".tmx");
        LoadLevel(level_to_load);
      }

      ammount = easeinout(selector->last_y, (selector->selected_level*50), fmin(1.0f, selector->anim_timer/0.25f));

      jamTextRenderSimple("River Fire.", true, hgeVec3(0, 200, 0), 1.0f, hgeVec4(1, 1, 1, 1));

      for(int i = 0; i < num_levels; i++) {
        float text_y = ammount + i*-50;
        jamTextRenderSimple(get_level_title(i), true, hgeVec3(0, text_y, 0), 1.0-fabs(text_y)/350.f, hgeVec4(1, 1, 1, 1.0-fabs(text_y)/150.f));
        level_data lvl_data = get_level_data(i);
        jam_time jt = float_to_int_time(lvl_data.time);
        sprintf(score_time_str, "%.2f%s %02d:%02d.%03d", lvl_data.score, "%", jt.minutes, jt.seconds, jt.miliseconds);
        jamTextRenderSimple(score_time_str, false, hgeVec3(25 + jamTextLengthOfText(get_level_title(i), 1.0-fabs(text_y)/350.f)/2, text_y+5, 1), 0.5-fabs(text_y)/350.f, hgeVec4(1, 1, 1, 0.75-fabs(text_y)/150.f));
      }
    break;
  }

	//jamTextRenderSimple("River Fire.", true, hgeVec3(0, 0, 0), 1.0f, hgeVec4(1, 1, 1, 0.25f));
	/*for(int i = 0; i < 5; i++) {
		jamTextRenderSimple("hi", true, hgeVec3(0, -100 - 50*i, 0), 1.0f, hgeVec4(1, 1, 1, 1));
	}*/
	hgeUseShader((hgeResourcesQueryShader("sprite_shader")));
}
