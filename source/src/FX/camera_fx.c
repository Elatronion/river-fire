#include "camera_fx.h"
#include "jamutils.h"

void System_CameraFX(hge_entity* entity, hge_vec3* position, camera_fx* camfx) {
  float shake_x = 20.f;
  if(hgeInputGetKeyDown(HGE_KEY_B)) camfx->shake_intensity = 5.f;
  //camfx->shake_intensity = 0;
  //camfx->shake_intensity = (camfx->shake_intensity - 0) * hgeDeltaTime();
  camfx->shake_intensity-= hgeDeltaTime();
  camfx->shake_intensity += -camfx->shake_intensity * 5 * hgeDeltaTime();
  camfx->shake_vector.x = RandomFloat(-camfx->shake_intensity, camfx->shake_intensity);
  camfx->shake_vector.y = RandomFloat(1, camfx->shake_intensity);
  camfx->shake_vector.z = RandomFloat(-camfx->shake_intensity, camfx->shake_intensity);
  camfx->desired_position.x += (camfx->shake_vector.x - camfx->desired_position.x) * hgeDeltaTime();
  camfx->desired_position.y += (camfx->shake_vector.y - camfx->desired_position.y) * shake_x * hgeDeltaTime();
  camfx->desired_position.z += (camfx->shake_vector.z - camfx->desired_position.z) * hgeDeltaTime();

  if(camfx->shake_intensity > 1.f) {
    position->x += camfx->desired_position.x;
    position->y = camfx->desired_position.y;
    position->z += camfx->desired_position.z;
  }
}

void shake_camera(float ammount) {
  camera_fx* requested_camfx;
  hge_ecs_request camfx_request = hgeECSRequest(1, "camera fx");
  for(int i = 0; i < camfx_request.NUM_ENTITIES; i++) {
    hge_entity* camera_entity = camfx_request.entities[i];
    requested_camfx = camera_entity->components[hgeQuery(camera_entity, "camera fx")].data;
  }

  requested_camfx->shake_intensity = ammount;
}
