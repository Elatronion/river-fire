#ifndef PARTICLES_H
#define PARTICLES_H

#define MAX_PARTICLES 500

#include <HGE/HGE_Core.h>

typedef struct {
  hge_vec3 position;
  hge_vec3 velocity;
  float lifetime;
} particle;

typedef struct {
  hge_mesh particle_mesh;
  unsigned int num_particles;
  particle particles[MAX_PARTICLES];
} particle_system;

void system_particles(hge_entity* entity, hge_vec3* position, particle_system* ps);

#endif
