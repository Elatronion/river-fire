#ifndef HGE_MATERIAL_H
#define HGE_MATERIAL_H

#include "HGE_Texture.h"

typedef struct {
  hge_texture diffuse, normal;
} hge_material;

#endif
